//
//  SwipeGestureViewController.swift
//  Gestures
//
//  Created by Danni Brito on 17/1/18.
//  Copyright © 2018 Danni Brito. All rights reserved.
//

import UIKit

class SwipeGestureViewController: UIViewController {

    @IBOutlet weak var customView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func swipeUpAction(_ sender: Any) {
       customView.backgroundColor = .red
    }
    @IBAction func swipeDownAction(_ sender: Any) {
        customView.backgroundColor = .yellow
    }
    @IBAction func swipeLeftAction(_ sender: Any) {
        customView.backgroundColor = .blue
    }
    @IBAction func swipeRightAction(_ sender: Any) {
        customView.backgroundColor = .black
    }
    

}
